<?php
namespace Drupal\allinone_backup\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Renderer;

/**
 * An allinone_backup controller.
 */
class BackupController extends ControllerBase implements ContainerInjectionInterface{

  /**
   * {@inheritdoc}
   */
  protected $renderer;
  public function __construct(Renderer $renderer) {
    $this->renderer = $renderer;
  }
  
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }
  public function Content() {
    $backupform = \Drupal::formBuilder()->getForm('\Drupal\allinone_backup\Form\backupForm');
    $content['logfieldset'] = array(
      '#type' => 'details',
      '#title' => $this->t('backup log'),
    );
    $content['logfieldset']['#markup'] = $this->renderer->render(views_embed_view('allinonebackup_block'));
    return [$backupform,$content];
  }
}
