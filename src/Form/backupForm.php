<?php
namespace Drupal\allinone_backup\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
/**
 * Configure allinone_backup settings for this site.
 */
class backupForm extends FormBase {
   public function mysql_escape_no_conn( $input ) // Escape data whene insert query run
  { 
      if( is_array( $input ) )
      {
          return array_map( __METHOD__, $input ); 
      }
      if( !empty( $input ) && is_string( $input ) ) 
      { 
          return str_replace( array( '\\', "\0", "\n", "\r", "'", '"', "\x1a" ), 
                              array( '\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z' ),
                              $input ); 
      } 

      return $input; 
  } 
  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'allinone_backup_form';
  }
  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $results = db_query("SHOW TABLES"); // Getting The List Of All Table for Exclude
   
    $tables = array();
    
    while($tablerow = $results->fetchObject())
    {
     $tablename = 'Tables_in_'.str_replace('/','',base_path());
      $tablelist[$tablerow->$tablename] = $tablerow->$tablename; 
    }

  	$form['backupfieldset'] = array(
		  '#type' => 'details',
		  '#title' => $this->t('Get backup now'),
      '#open'=>TRUE,
	  );
    $form['backupfieldset']['exclude_tablelist'] = array(
        '#title' => t('Tables To Exclude'),
        '#type' => 'select',
        '#size' => 10,        
        //'#prefix' => '',
        //'#suffix' => '<a id="selectall">Select All </a><br>', 
        '#multiple' => true,
        '#options' => $tablelist
    );
    $form['backupfieldset']['actions']['submit']  = array(
      '#type' => 'submit',
      '#prefix' => '<br>',
      '#value' => $this->t('Backup Now'),
      '#button_type' => 'primary',
      '#weight' => '100',
    );
    return  $form;
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
      $limit = 200;
      $db = \Drupal\Core\Database\Database::getConnectionInfo();

      $new_folder = 'public://allinone/';

      if (!is_dir ( drupal_realpath($new_folder) )) {
  
         $url = file_prepare_directory($new_folder, FILE_CREATE_DIRECTORY);
  
       } 
      $excluded = $form_state->getValue('exclude_tablelist');    // Get The excluded Tables List 
      $dbname = $db['default']['database']; // Get Current Database authenication for backup
      $username = $db['default']['username'];
      $password = $db['default']['password'];
      $host = $db['default']['host'];

      $params = array(

        'db_host'=> $host,  

        'db_uname' => $username, 

        'db_password' => $password, 

        'db_to_backup' => $dbname, 

        'db_exclude_tables' => $excluded, //Put The Tables Name That We Won't To Backup

       );       
        // ==========    IF exec not working on server ========== //
        $mtables = array(); 
        $contents = "-- Database: `".$params['db_to_backup']."` --\n";
         
        $results = db_query("SHOW TABLES"); // Getting The List Of All Table
      
        while($row = $results->fetchObject())
        {
          $rowstable = (array) $row;
          $rowstable = array_values($rowstable);
          if (!in_array($rowstable[0], $params['db_exclude_tables']))   //Filter Tables = Total Tables - Exclude Table 
          {
             $mtables[] = $rowstable[0];
          }
        }

         foreach($mtables as $table) // Loop The All Table Structure And Data
        {
            
            $contents .= "\n\n--    Table Strucure For Table  `".$table."`    --\n\n";

            $contents .="DROP TABLE IF EXISTS `".$table."`;\n\n";  // Drop If Exists Table Query 

            $results = db_query("SHOW CREATE TABLE ".$table);   // Create Table Query 

            
            while($row = $results->fetchAssoc())
            {
                $row = array_values($row);

                $contents .= str_replace('"','`',$row[1]).";\n\n";  // created table query
            }

            $results = db_query("SELECT * FROM ".$table);  // Select Current Table Data For Insertion

            $results->allowRowCount = TRUE;

            $row_count = $results->rowCount();

            $fields = $results->fetchAssoc();
         
            $fields = array_keys($fields);
               
            $fields_count = count($fields);
           
            if($row_count>0) // Insert Query Run If Records Is Available
            {

                  $insert_head = "INSERT INTO `".$table."` ("; // Insert Data Query 

                  for($i=0; $i < $fields_count; $i++)
                  {
                          $insert_head  .= "`".$fields[$i]."`";
                          
                          if($i < $fields_count-1)
                          {
                                  $insert_head  .= ', ';
                          }
                  }
                  $insert_head .=  ")";

                  $insert_head .= " VALUES\n";        
                  
                  $r = 1;

                  while($row = $results->fetchAssoc())
                  {

                      $row = array_values($row);

                      if(  $r == 1  ||   ($r % $limit)  == 0)
                      {
                          $contents .= $insert_head;
                      }
                      
                      $contents .= "(";
                      
                      for($i=0; $i < $fields_count; $i++)
                      {
                          $row_content = $row[$i];

                          $contents .= "'" . $this->mysql_escape_no_conn(trim($row_content))  .  "'";  //  Escape Data 

                            if($i < $fields_count-1)
                            {
                                    $contents  .= ', ';
                            }
                            else
                            {
                              $contents  .= ' ';
                            }
                      }
                      
                     if( ($r+1) == $row_count || ($r % $limit) == $limit-1)
                      {
                          $contents .= ");\n\n ";

                      }
                      else
                      {
                          $contents .= "),\n";
                      }
                      $r++;
                  }

            }
          
          } 
          $datestamp = date("Y-m-d-his");
          $backup_file_name_create = $dbname."-".$datestamp.".sql";

          $fp = fopen(drupal_realpath($new_folder)."/".$backup_file_name_create ,'w+');   
          
          if (($result = fwrite($fp, $contents))) 
          {
              chmod(('sites/default/files/allinone/'.$backup_file_name_create),  0777);  // Change Permission to all user access
              chmod('sites/default/files/allinone',  0777);  // Change Permission to all user access
              drupal_set_message("Backup $backup_file_name_create file successfully");
          }

          fclose($fp);
          
          $zip = new \ZipArchive();   // Archive The Created File
          $backup_file_name = $dbname."-".mt_rand(0,100).".zip" ;
          $fp = drupal_realpath($new_folder)."/".$backup_file_name_create;
          if ($zip->open( drupal_realpath($new_folder).'/'.$backup_file_name, \ZIPARCHIVE::CREATE)!==TRUE) {

               $zipresponce = ("cannot open <$filename>\n");
          }

          $zip->addFromString($backup_file_name_create , file_get_contents($fp));

          $zip->close();
          unlink($fp);
          // ==========    Add Content For Database Backup History Display ========== //
          $node = Node::create([
              'type'        => 'backup',
              'title'       => $backup_file_name,
            ]);
          $node->save();
    
  }
}